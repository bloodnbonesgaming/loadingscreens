package me.platypus.loadingscreens.client;

import me.platypus.loadingscreens.client.config.ConfigManager;
import me.platypus.loadingscreens.client.gui.screens.ConfigScreen;
import net.minecraftforge.client.ConfigScreenHandler.ConfigScreenFactory;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.ModLoadingContext;

public class ClientSetup {

	public static void setup() {
		//Add our config screen
        ModLoadingContext.get().getActiveContainer().registerExtensionPoint(ConfigScreenFactory.class, () -> {
        	return new ConfigScreenFactory((mc, screen) -> {
            	return new ConfigScreen(ConfigManager.load());
            });
        });
    	//Load the config
        ConfigManager.load();
        //Register client events
        MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
	}
}
