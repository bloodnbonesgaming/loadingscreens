package me.platypus.loadingscreens.client;

import java.io.IOException;
import java.io.InputStream;

import com.mojang.blaze3d.platform.NativeImage;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;

public class ImageUtil {

	public static NativeImage loadNativeImage(ResourceManager manager, ResourceLocation location) {
		NativeImage nativeImage = null;
		
        try {
           Resource resource = manager.getResourceOrThrow(location);

           try (InputStream inputstream = resource.open()) {
        	   nativeImage = NativeImage.read(inputstream);
           }
        } catch (IOException ioexception) {
           
        }
        return nativeImage;
     }
}
