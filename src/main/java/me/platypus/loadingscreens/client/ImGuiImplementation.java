package me.platypus.loadingscreens.client;

import imgui.ImGui;
import imgui.ImGuiIO;
import imgui.gl3.ImGuiImplGl3;
import imgui.glfw.ImGuiImplGlfw;
import net.minecraft.client.Minecraft;

public class ImGuiImplementation {
	
    private static ImGuiImplGlfw imGuiGlfw;
    private static ImGuiImplGl3 imGuiGl3;
    private static boolean initialized = false;

	public static boolean initImGui() {
		if (!initialized) {
			imGuiGlfw = new ImGuiImplGlfw();
			imGuiGl3 = new ImGuiImplGl3();
			ImGui.createContext();
	        imGuiGlfw.init(Minecraft.getInstance().getWindow().getWindow(), true);
	        imGuiGl3.init("#version 150");
	        
	        ImGuiIO io = ImGui.getIO();
	        io.setIniFilename(null);

			initialized = true;
	        return true;
		}
		return false;
	}
	
	public static void disposeImGui() {
		if (initialized) {
			initialized = false;
			imGuiGlfw.dispose();
			imGuiGl3.dispose();
			ImGui.destroyContext();
		}
	}
	
	public static void startFrame() {
		imGuiGlfw.newFrame();
        ImGui.newFrame();
	}
	
	public static void endFrame() {
		ImGui.render();
        imGuiGl3.renderDrawData(ImGui.getDrawData());

//        if (ImGui.getIO().hasConfigFlags(ImGuiConfigFlags.ViewportsEnable)) {
//            final long backupWindowPtr = GLFW.glfwGetCurrentContext();
//            ImGui.updatePlatformWindows();
//            ImGui.renderPlatformWindowsDefault();
//            GLFW.glfwMakeContextCurrent(backupWindowPtr);
//        }
	}
}
