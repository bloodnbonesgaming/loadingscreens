package me.platypus.loadingscreens.client.config;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import me.platypus.loadingscreens.LoadingScreens;
import me.platypus.loadingscreens.client.gui.component.Anchor;
import me.platypus.loadingscreens.client.gui.component.ColorComponent;
import me.platypus.loadingscreens.client.gui.component.ComponentBase;
import me.platypus.loadingscreens.client.gui.component.HorizontalAlignment;
import me.platypus.loadingscreens.client.gui.component.TextComponent;
import me.platypus.loadingscreens.client.gui.component.TextureComponent;
import me.platypus.loadingscreens.client.gui.component.VerticalAlignment;

public class ConfigManager {

	public static final Gson GSON = new Gson().newBuilder().setPrettyPrinting().registerTypeAdapter(ComponentBase.class, new ComponentBase.Deserializer()).create();
	public static List<ComponentBase> components = new ArrayList<ComponentBase>();
	
	public static final File configFolder = new File("./config/"+LoadingScreens.MODID+"/");
	public static final File configFile = new File(configFolder, "Components.json");
	public static final File readmeFile = new File(configFolder, "README.txt");
	private static final Random rand = new Random();
	
	/*
	 * Randomize the text option index of every text component so it's likely to be different next time a loading screen opens
	 */
	public static void randomizeText() {
		for (ComponentBase comp : components) {
			if (comp instanceof TextComponent txt) {
				if (txt.textOptions.size() > 1) {
					txt.textOptionIndex = rand.nextInt(txt.textOptions.size());
				}
			}
		}
	}
	
	public static void save(List<ComponentBase> components) {
		saveInternal(components);
		load();
	}
	
	public static List<ComponentBase> load() {
		List<ComponentBase> components = new ArrayList<ComponentBase>();
		configFolder.mkdirs();
		
		if (!configFile.exists()) {
			createDefaultConfig();
		}
		
		try {
			String fromFile = Files.asCharSource(configFile, StandardCharsets.UTF_8).read();
			List<ComponentBase> fromJson = GSON.fromJson(fromFile, new TypeToken<List<ComponentBase>>() {}.getType());
			
			if (fromJson != null && fromJson.size() > 0) {
				for (ComponentBase comp : fromJson) {
					comp.onLoad();
				}
				components.addAll(fromJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		ConfigManager.components = components;
		return components;
	}
	
	private static void saveInternal(List<ComponentBase> components) {
		configFolder.mkdirs();
		
		try {
			Files.asCharSink(configFile, StandardCharsets.UTF_8).write(GSON.toJson(components));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void createDefaultConfig() {
		List<ComponentBase> components = new ArrayList<ComponentBase>();
		
		//Flat black color to act as the background
		ColorComponent baseBlack = (ColorComponent) ComponentBase.ComponentTypes.COLOR.create();
		baseBlack.size.width = 0;
		baseBlack.size.height = 0;
		baseBlack.size.relWidth = 1.0F;
		baseBlack.size.relHeight = 1.0F;
		
		baseBlack.color = new float[] {0.0F, 0.0F, 0.0F, 1.0F};
		
		baseBlack.position.setAnchor(Anchor.TOP_LEFT);
		baseBlack.position.horizontalAlignment = HorizontalAlignment.LEFT;
		baseBlack.position.verticalAlignment = VerticalAlignment.TOP;
		baseBlack.position.x = 0;
		baseBlack.position.z = 0;
		baseBlack.position.relX = 0.0F;
		baseBlack.position.relZ = 0.0F;
		
		components.add(baseBlack);
		
		
		//Vanilla fighters painting
		TextureComponent painting = (TextureComponent) ComponentBase.ComponentTypes.TEXTURE.create();
		painting.size.width = 0;
		painting.size.height = 0;
		painting.size.relWidth = 0.8F;
		painting.size.relHeight = 0.8F;
		
		painting.texturePath = "minecraft:textures/painting/fighters.png";
		painting.tile = false;
		painting.tileWidth = 32;
		painting.tileHeight = 32;
		
		painting.position.setAnchor(Anchor.CENTER);
		painting.position.horizontalAlignment = HorizontalAlignment.CENTER;
		painting.position.verticalAlignment = VerticalAlignment.CENTER;
		painting.position.x = 0;
		painting.position.z = 0;
		painting.position.relX = 0.0F;
		painting.position.relZ = 0.0F;
		
		components.add(painting);
		
		
		//Tip telling the dev where to find the ui
		TextComponent tips = (TextComponent) ComponentBase.ComponentTypes.TEXT.create();
		List<String[]> textOptions = new ArrayList<String[]>();
		textOptions.add(new String[] {
				"\\u00A7eTip:",
		        "To configure Loading Screens, navigate from the main menu to the mod list,",
		        "select the Loading Screens mod, hit the config button and use the gui"
		});
		tips.textOptions = textOptions;

		tips.color = new float[] {1.0F, 1.0F, 1.0F, 1.0F};
		
		tips.position.setAnchor(Anchor.BOTTOM_LEFT);
		tips.position.horizontalAlignment = HorizontalAlignment.LEFT;
		tips.position.verticalAlignment = VerticalAlignment.BOTTOM;
		tips.position.x = 10;
		tips.position.z = -10;
		tips.position.relX = 0.0F;
		tips.position.relZ = 0.0F;
		
		components.add(tips);
		
		//Save defaults
		saveInternal(components);
		createREADME();
	}
	
	private static void createREADME() {
		configFolder.mkdirs();
		
		try {
			Files.asCharSink(readmeFile, StandardCharsets.UTF_8).write("Do not edit the json files manually. Use the mod config gui accessible from the mod menu which itself is accessible from the main menu.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
