package me.platypus.loadingscreens.client.gui.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import imgui.ImGui;
import imgui.flag.ImGuiCond;
import imgui.type.ImInt;
import me.platypus.loadingscreens.client.ImGuiImplementation;
import me.platypus.loadingscreens.client.config.ConfigManager;
import me.platypus.loadingscreens.client.gui.component.ComponentBase;
import me.platypus.loadingscreens.client.gui.component.ComponentBase.ComponentTypes;
import me.platypus.loadingscreens.client.gui.component.TextureComponent;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;

public class ConfigScreen extends Screen {
	
	public List<ComponentBase> components = new ArrayList<ComponentBase>();
    private List<ComponentBase> componentsToRemove = new ArrayList<ComponentBase>();
    private List<ComponentBase> componentsToAdd = new ArrayList<ComponentBase>();
    private List<ComponentBase> componentsToMoveUp = new ArrayList<ComponentBase>();
    private List<ComponentBase> componentsToMoveDown = new ArrayList<ComponentBase>();
	private boolean close = false;
	private static final TextureComponent thisIsDumb = new TextureComponent();
	
	static {
		thisIsDumb.texture = new ResourceLocation("minecraft:textures/block/iron_ore.png");
		thisIsDumb.size.relWidth = 0F;
		thisIsDumb.size.relHeight = 0F;
	}

	public ConfigScreen() {
		super(Component.literal("Loading Screens Config"));
		
		addDefaultBackground();
	}

	public ConfigScreen(List<ComponentBase> components) {
		super(Component.literal("Loading Screens Config"));
		
		this.components = components;
		
		//If there's somehow no components, add a default one
		if (this.components.size() < 1) {
			addDefaultBackground();
		}
	}
	
	private void addDefaultBackground() {
		TextureComponent background = (TextureComponent) ComponentBase.ComponentTypes.TEXTURE.create();
		background.texture = new ResourceLocation("minecraft:textures/block/iron_ore.png");
		background.size.relWidth = 1F;
		background.size.relHeight = 1F;
		background.tile = true;
		
		this.components.add(background);
	}
	
	@Override
	protected void init() {
		super.init();
	}
	
	@Override
	public boolean shouldCloseOnEsc() {
		return false;
	}
	
	@Override
	public void onClose() {
		super.onClose();
	}
	
	@Override
	public void render(GuiGraphics graphics, int x, int z, float partialTick) {
		
		for (ComponentBase comp : this.components) {
			comp.render(graphics, x, z, this.width, this.height, partialTick);
		}
		//Something about the texture component fixes the rendering, so need to have one before the imgui calls or it all breaks
		thisIsDumb.render(graphics, x, z, this.width, this.height, partialTick);

		ImGuiImplementation.initImGui();
		ImGuiImplementation.startFrame();
		
		ImGui.setNextWindowSize(450, 400, ImGuiCond.FirstUseEver);
		
		ImGui.begin("Loading Screen Configuration");
                
        if (ImGui.button("Save & Exit")) {
        	ConfigManager.save(this.components);
        	this.close = true;
        }
        ImGui.sameLine();
        
        if (ImGui.button("Discard & Exit")) {
        	this.close = true;
        }
        
        if (ImGui.treeNode("Components")) {
        	
        	if (ImGui.beginPopup("New Component")) {
        		ImInt typeIndex = new ImInt(0);
        		
    			if (ImGui.combo("Type", typeIndex, ComponentBase.ComponentTypes.names())) {
    				ComponentTypes type = ComponentBase.ComponentTypes.values()[typeIndex.get()];
    				
    				this.components.add(type.create());
    				ImGui.closeCurrentPopup();
    			}
    			ImGui.endPopup();
    		}
        	
            if (ImGui.button("New Component")) {
    			ImGui.openPopup("New Component");
            }
            
        	for (ComponentBase comp : this.components) {
        		if (ImGui.treeNode(comp.imGuiLabel()+"##"+comp.uuid)) {
        			
        			if (ImGui.button("Move Up")) {
        				if (this.components.indexOf(comp) > 0) {
        					this.componentsToMoveUp.add(comp);
        				}
        			}
        			ImGui.sameLine();
        			
        			if (ImGui.button("Move Down")) {
        				if (this.components.indexOf(comp) < this.components.size() - 1) {
        					this.componentsToMoveDown.add(comp);
        				}
        			}
        			ImGui.sameLine();
        			
        			if (ImGui.button("Clone")) {
        				this.componentsToAdd.add(comp.clone());
        			}
        			ImGui.sameLine();
        			
        			if (ImGui.button("Delete")) {
        				this.componentsToRemove.add(comp);
        			}
        			
        			comp.imGui();
                	ImGui.treePop();
                }
    		}
        	
        	ImGui.treePop();
        }
        
        ImGui.end();

		ImGuiImplementation.endFrame();
		if (this.close) {
			this.onClose();
			return;
		}
		
		//Move components up
		for (ComponentBase comp : this.componentsToMoveUp) {
			int index = this.components.indexOf(comp);
			Collections.swap(this.components, this.components.indexOf(comp), Math.max(0, index - 1));
		}
		this.componentsToMoveUp.clear();
		
		//Move components down
		for (ComponentBase comp : this.componentsToMoveDown) {
			int index = this.components.indexOf(comp);
			Collections.swap(this.components, this.components.indexOf(comp), Math.min(this.components.size() - 1, index + 1));
		}
		this.componentsToMoveDown.clear();
		
		//Remove deleted components
		for (ComponentBase comp : this.componentsToRemove) {
			this.components.remove(comp);
		}
		this.componentsToRemove.clear();
		
		//Add cloned components
		this.components.addAll(componentsToAdd);
		this.componentsToAdd.clear();
	}
}
