package me.platypus.loadingscreens.client.gui.component;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.UUID;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import me.platypus.loadingscreens.client.config.ConfigManager;
import net.minecraft.client.gui.GuiGraphics;

public abstract class ComponentBase {

	public ComponentTypes type;
	public Position position = new Position();
	public final transient String uuid = UUID.randomUUID().toString();
	
	public abstract void render(GuiGraphics graphics, int x, int z, int uiWidth, int uiHeight, float partialTick);
	public abstract void imGui();
	public abstract String imGuiLabel();
	public abstract ComponentBase clone();
	
	public static enum ComponentTypes {
		TEXTURE {
			@Override
			public ComponentBase create() {
				ComponentBase comp = new TextureComponent();
				comp.type = this;
				return comp;
			}
			
			@Override
			public String toString() {
				return "Texture";
			}

			@Override
			public Class<? extends ComponentBase> getComponentClass() {
				return TextureComponent.class;
			}
		},
		COLOR {
			@Override
			public ComponentBase create() {
				ComponentBase comp = new ColorComponent();
				comp.type = this;
				return comp;
			}
			
			@Override
			public String toString() {
				return "Color";
			}

			@Override
			public Class<? extends ComponentBase> getComponentClass() {
				return ColorComponent.class;
			}
		},
		TEXT {
			@Override
			public ComponentBase create() {
				ComponentBase comp = new TextComponent();
				comp.type = this;
				return comp;
			}
			
			@Override
			public String toString() {
				return "Text";
			}

			@Override
			public Class<? extends ComponentBase> getComponentClass() {
				return TextComponent.class;
			}
		};
		
		public static String[] names() {
			return Arrays.asList(ComponentBase.ComponentTypes.values()).stream().map((s) -> s.toString()).toArray(String[]::new);
		}

		private ComponentTypes() {
			
		}
		
		public abstract ComponentBase create();
		public abstract Class<? extends ComponentBase> getComponentClass();
	}

	public void onLoad() {
		
	}
	
	public static class Deserializer implements JsonDeserializer<ComponentBase> {
		@Override
		public ComponentBase deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			JsonObject obj = json.getAsJsonObject();
			String typeString = obj.get("type").getAsString();
			ComponentBase.ComponentTypes type = ComponentTypes.valueOf(typeString);
			
			return ConfigManager.GSON.fromJson(json, type.getComponentClass());
		}
	}
}
