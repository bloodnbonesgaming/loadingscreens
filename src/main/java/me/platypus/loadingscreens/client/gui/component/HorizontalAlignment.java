package me.platypus.loadingscreens.client.gui.component;

import java.util.Arrays;

public enum HorizontalAlignment {
	LEFT {
		@Override
		public String toString() {
			return "Left";
		}

		@Override
		public int getXOffsetFromWidth(int width) {
			return 0;
		}
	},
	RIGHT {
		@Override
		public String toString() {
			return "Right";
		}

		@Override
		public int getXOffsetFromWidth(int width) {
			return -width;
		}
	},
	CENTER {
		@Override
		public String toString() {
			return "Center";
		}

		@Override
		public int getXOffsetFromWidth(int width) {
			return -(width / 2);
		}
	};
	
	public abstract int getXOffsetFromWidth(int width);
	
	public static String[] names() {
		return Arrays.asList(values()).stream().map((s) -> s.toString()).toArray(String[]::new);
	}
}
