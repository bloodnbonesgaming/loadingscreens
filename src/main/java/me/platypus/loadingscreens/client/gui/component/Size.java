package me.platypus.loadingscreens.client.gui.component;

import imgui.ImGui;

public class Size {
	public int width = 0;
	public int height = 0;
	public float relWidth = 0.25F;
	public float relHeight = 0.25F;
	
	public int width(int uiWidth) {
		return this.width + (int)(uiWidth * this.relWidth);
	}
	
	public int height(int uiHeight) {
		return this.height + (int)(uiHeight * this.relHeight);
	}
	
	public void imGui() {
		ImGui.pushItemWidth(ImGui.getWindowWidth() * 0.45F);
		
    	//Absolute size
    	int[] width = new int[] {this.width};
		int[] height = new int[] {this.height};
		
    	if (ImGui.dragInt("Absolute Width", width, 1, 0, 4000)) {
    		this.width = width[0];
    	}
		
    	if (ImGui.dragInt("Absolute Height", height, 1, 0, 4000)) {
    		this.height = height[0];
    	}
    	
    	//Relative size
    	float[] relWidth = new float[] { this.relWidth };
    	float[] relHeight = new float[] { this.relHeight };
    	
    	if (ImGui.dragFloat("Relative Width", relWidth, 0.0025F, 0F, 1F)) {
    		this.relWidth = relWidth[0];
    	}
    	
    	if (ImGui.dragFloat("Relative Height", relHeight, 0.0025F, 0F, 1F)) {
    		this.relHeight = relHeight[0];
    	}
    	
    	ImGui.popItemWidth();
	}

	@Override
	public Size clone() {
		Size size = new Size();
		
		size.width = this.width;
		size.height = this.height;
		size.relWidth = this.relWidth;
		size.relHeight = this.relHeight;
		
		return size;
	}
}
