package me.platypus.loadingscreens.client.gui.component;

import java.util.Arrays;

public enum VerticalAlignment {
	TOP {
		@Override
		public String toString() {
			return "Top";
		}

		@Override
		public int getZOffsetFromHeight(int height) {
			return 0;
		}
	},
	BOTTOM {
		@Override
		public String toString() {
			return "Bottom";
		}

		@Override
		public int getZOffsetFromHeight(int height) {
			return -height;
		}
	},
	CENTER {
		@Override
		public String toString() {
			return "Center";
		}

		@Override
		public int getZOffsetFromHeight(int height) {
			return -(height / 2);
		}
	};
	
	public abstract int getZOffsetFromHeight(int height);
	
	public static String[] names() {
		return Arrays.asList(values()).stream().map((s) -> s.toString()).toArray(String[]::new);
	}
}
