package me.platypus.loadingscreens.client.gui.component;

import imgui.ImGui;
import imgui.flag.ImGuiColorEditFlags;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.util.FastColor;

public class ColorComponent extends ComponentBase {
	
	public Size size = new Size();
	public float[] color = new float[] {0, 0, 0, 1};

	@Override
	public void render(GuiGraphics graphics, int x, int z, int uiWidth, int uiHeight, float partialTick) {
		int width = this.size.width(uiWidth);
		int height = this.size.height(uiHeight);
		int xPos = this.position.getXPos(uiWidth, width);
		int zPos = this.position.getZPos(uiHeight, height);
		
		int minX = xPos;
		int minZ = zPos;
		int maxX = minX + width;
		int maxZ = minZ + height;
		
		graphics.fill(minX, minZ, maxX, maxZ, FastColor.ARGB32.color((int)(color[3] * 255), (int)(color[0] * 255), (int)(color[1] * 255), (int)(color[2] * 255)));
	}

	@Override
	public void imGui() {
		//Position
    	if (ImGui.treeNode("Position")) {
			this.position.imGui();
        	ImGui.treePop();
        }
    	
    	//Size
    	if (ImGui.treeNode("Size")) {
        	this.size.imGui();
        	ImGui.treePop();
        }
    	
    	//Color
    	if (ImGui.colorEdit4("Color", this.color, ImGuiColorEditFlags.AlphaPreviewHalf)) {
    		
    	}
	}

	@Override
	public String imGuiLabel() {
		return "Color";
	}

	@Override
	public ComponentBase clone() {
		ColorComponent comp = (ColorComponent) ComponentTypes.COLOR.create();
		comp.position = this.position.clone();
		
		comp.size = this.size.clone();
		comp.color = this.color.clone();

		comp.onLoad();
		return comp;
	}
}
