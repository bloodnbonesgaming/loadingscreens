package me.platypus.loadingscreens.client.gui.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import imgui.ImGui;
import imgui.flag.ImGuiColorEditFlags;
import imgui.type.ImString;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.util.FastColor;

public class TextComponent extends ComponentBase {
	
	public List<String[]> textOptions = new ArrayList<String[]>();
	public transient int textOptionIndex = 0;
	public float[] color = new float[] {1, 1, 1, 1};
	
	public TextComponent() {
		this.textOptions.add(new String[] {});
	}

	@Override
	public void render(GuiGraphics graphics, int x, int z, int uiWidth, int uiHeight, float partialTick) {
		Font font = Minecraft.getInstance().font;
		
		if (this.textOptions.size() < 1) {
			return;
		}
		String[] lines = this.textOptions.get(Math.min(textOptionIndex, this.textOptions.size() - 1));
		
		if (lines == null) {
			return;
		}
		int width = 0;
		int height = lines.length * font.lineHeight;
		
		for (int i = 0; i < lines.length; i++) {
			String text = StringEscapeUtils.unescapeJava(lines[i]);
			int length = font.width(text);
			
			if (length > width) {
				width = length;
			}
		}
		
		switch (this.position.horizontalAlignment) {
			case CENTER: {
				int lineOffset = 0;

				for (int i = 0; i < lines.length; i++) {
					String text = StringEscapeUtils.unescapeJava(lines[i]);

					if (text != null && !text.isBlank()) {
						graphics.drawCenteredString(font, text, this.position.getXPos(uiWidth, 0), this.position.getZPos(uiHeight, height) + lineOffset, FastColor.ARGB32.color((int) (color[3] * 255), (int) (color[0] * 255), (int) (color[1] * 255), (int) (color[2] * 255)));
					}
					lineOffset += font.lineHeight;
				}
				break;
			}
			case LEFT: {
				int lineOffset = 0;

				for (int i = 0; i < lines.length; i++) {
					String text = StringEscapeUtils.unescapeJava(lines[i]);

					if (text != null && !text.isBlank()) {
						graphics.drawString(font, text, this.position.getXPos(uiWidth, width), this.position.getZPos(uiHeight, height) + lineOffset, FastColor.ARGB32.color((int) (color[3] * 255), (int) (color[0] * 255), (int) (color[1] * 255), (int) (color[2] * 255)));
					}
					lineOffset += font.lineHeight;
				}
				break;
			}
			case RIGHT: {
				int lineOffset = 0;

				for (int i = 0; i < lines.length; i++) {
					String text = StringEscapeUtils.unescapeJava(lines[i]);
					int textWidth = font.width(text);

					if (text != null && !text.isBlank()) {
						graphics.drawString(font, text, this.position.getXPos(uiWidth, width) + width - textWidth, this.position.getZPos(uiHeight, height) + lineOffset, FastColor.ARGB32.color((int) (color[3] * 255), (int) (color[0] * 255), (int) (color[1] * 255), (int) (color[2] * 255)));
					}
					lineOffset += font.lineHeight;
				}
				break;
			}
		}
	}

	@Override
	public void imGui() {
		//Position
    	if (ImGui.treeNode("Position")) {
			this.position.imGui();
        	ImGui.treePop();
        }
    	
    	//Text
    	if (ImGui.treeNode("Text Options")) {
	        if (ImGui.button("Add")) {
				this.textOptions.add(new String[] {});
	        }
			for (int o = 0; o < this.textOptions.size(); o++) {
				if (ImGui.treeNode("Option "+(o+1))) {
					if (this.textOptions.size() > 1) {
				        if (ImGui.button("Remove")) {
							this.textOptions.remove(o);
				        }
					}
			        
					String[] lines = this.textOptions.get(o);
//					String textCombined = "";
					StringBuilder textCombined = new StringBuilder();
					
					for (int i = 0; i < lines.length; i++) {
						if (i != 0) {
							textCombined.append("\n");
//							textCombined = textCombined + "\n";
						}
						textCombined.append(lines[i]);
//						textCombined = textCombined + lines[i];
					}
//					LoadingScreens.LOGGER.info("Text3: "+ textCombined);
					ImString text = new ImString(textCombined.toString(), 5000);
//					ImString text = new ImString(textCombined, 5000);
					
					if (ImGui.inputTextMultiline("Text", text)) {
						if (ImGui.isItemActive()) {
							this.textOptions.set(o, text.get().split("\n"));
						}
					}
					if (ImGui.isItemActive()) {
						this.textOptionIndex = o;
					}
		        	ImGui.treePop();
		        }
			}
        	ImGui.treePop();
        }
		
		//Color
		ImGui.colorEdit4("Color", this.color, ImGuiColorEditFlags.AlphaPreviewHalf);
	}

	@Override
	public String imGuiLabel() {
		return "Text";
	}

	@Override
	public ComponentBase clone() {
		TextComponent comp = (TextComponent) ComponentTypes.TEXT.create();
		comp.position = this.position.clone();
		
		List<String[]> textOptions = new ArrayList<String[]>();
		textOptions.addAll(this.textOptions.stream().map(s -> s.clone()).toList());
		comp.textOptions = textOptions;
		
		comp.textOptionIndex = 0;
		comp.color = this.color.clone();

		comp.onLoad();
		return comp;
	}
}
