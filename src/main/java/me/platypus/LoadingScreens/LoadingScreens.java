package me.platypus.loadingscreens;

import org.slf4j.Logger;

import com.mojang.logging.LogUtils;

import me.platypus.loadingscreens.client.ClientSetup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod(LoadingScreens.MODID)
public class LoadingScreens {
	public static final String MODID = "loadingscreens";
	public static final Logger LOGGER = LogUtils.getLogger();

	public LoadingScreens() {
	}
	
	@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
	public static class ClientModEvents {
		@SubscribeEvent
		public static void onClientSetup(FMLClientSetupEvent event) {
			ClientSetup.setup();
		}
	}
}
