package me.platypus.loadingscreens.client.gui.component;

import imgui.ImGui;
import imgui.type.ImInt;

public class Position {

	private Anchor anchor = Anchor.TOP_LEFT;
	public HorizontalAlignment horizontalAlignment = HorizontalAlignment.LEFT;
	public VerticalAlignment verticalAlignment = VerticalAlignment.TOP;
	public int x;
	public int z;
	public float relX;
	public float relZ;
	
	public int getXPos(int uiWidth, int width) {
		return this.anchor.getX(this, uiWidth) + this.horizontalAlignment.getXOffsetFromWidth(width);
	}
	
	public int getZPos(int uiHeight, int height) {
		return this.anchor.getZ(this, uiHeight) + this.verticalAlignment.getZOffsetFromHeight(height);
	}
	
	public void setAnchor(Anchor anchor) {
		this.anchor = anchor;
	}
	
	public void imGui() {
		ImGui.pushItemWidth(ImGui.getWindowWidth() * 0.45F);
		
		//Anchor
		ImInt anchorIndex = new ImInt(this.anchor.ordinal());
		
		if (ImGui.combo("Anchor", anchorIndex, Anchor.names())) {
			this.setAnchor(Anchor.values()[anchorIndex.get()]);
		}
		
		//Alignment
		ImInt horizontalAlignmentIndex = new ImInt(this.horizontalAlignment.ordinal());
		ImInt verticalAlignmentIndex = new ImInt(this.verticalAlignment.ordinal());
		
		if (ImGui.combo("Horizontal Alignment", horizontalAlignmentIndex, HorizontalAlignment.names())) {
			this.horizontalAlignment = HorizontalAlignment.values()[horizontalAlignmentIndex.get()];
		}
		
		if (ImGui.combo("Vertical Alignment", verticalAlignmentIndex, VerticalAlignment.names())) {
			this.verticalAlignment = VerticalAlignment.values()[verticalAlignmentIndex.get()];
		}
		
		//Absolute position
		int[] x = new int[] {this.x};
		int[] z = new int[] {this.z};
		
    	if (ImGui.dragInt("X", x, 1, -4000, 4000)) {
    		this.x = x[0];
    	}
		
    	if (ImGui.dragInt("Z", z, 1, -4000, 4000)) {
    		this.z = z[0];
    	}
    	
    	//Relative position
    	float[] relX = new float[] { this.relX };
    	float[] relZ = new float[] { this.relZ };
    	
    	if (ImGui.dragFloat("Relative X", relX, 0.0025F, -1F, 1F)) {
    		this.relX = relX[0];
    	}
    	
    	if (ImGui.dragFloat("Relative Z", relZ, 0.0025F, -1F, 1F)) {
    		this.relZ = relZ[0];
    	}
    	
    	ImGui.popItemWidth();
	}
	
	public Position clone() {
		Position pos = new Position();
		
		pos.anchor = this.anchor;
		pos.horizontalAlignment = this.horizontalAlignment;
		pos.verticalAlignment = this.verticalAlignment;
		pos.x = this.x;
		pos.z = this.z;
		pos.relX = this.relX;
		pos.relZ = this.relZ;
		
		return pos;
	}
}
