package me.platypus.loadingscreens.client.gui.screens;

import java.util.List;

import me.platypus.loadingscreens.client.config.ConfigManager;
import me.platypus.loadingscreens.client.gui.component.ComponentBase;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ProgressScreen;

public class ProgressScreenOverride extends ProgressScreen {
	
	private final List<ComponentBase> components;

	public ProgressScreenOverride(boolean clearScreenAfterStop) {
		super(clearScreenAfterStop);
		this.components = ConfigManager.components;
	}

	@Override
	public void render(GuiGraphics graphics, int x, int z, float partialTick) {
		for (ComponentBase comp : this.components) {
			comp.render(graphics, x, z, this.width, this.height, partialTick);
		}
		super.render(graphics, x, z, partialTick);
	}

	@Override
	public void renderBackground(GuiGraphics graphics) {
		
	}
	
	@Override
	public void renderDirtBackground(GuiGraphics p_282281_) {
		
	}
}
