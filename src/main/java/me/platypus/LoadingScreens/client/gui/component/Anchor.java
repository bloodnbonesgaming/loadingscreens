package me.platypus.loadingscreens.client.gui.component;

import java.util.Arrays;

public enum Anchor {
	TOP_LEFT("Top Left") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ);
		}
	},
	TOP("Top Center") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + (int)(uiWidth * 0.5F);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ);
		}
	},
	TOP_RIGHT("Top Right") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + uiWidth;
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ);
		}
	},
	LEFT("Left Center") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + (int)(uiHeight * 0.5F);
		}
	},
	CENTER("Center") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + (int)(uiWidth * 0.5F);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + (int)(uiHeight * 0.5F);
		}
	},
	RIGHT("Right") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + uiWidth;
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + (int)(uiHeight * 0.5F);
		}
	},
	BOTTOM_LEFT("Bottom Left") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + uiHeight;
		}
	},
	BOTTOM("Bottom Center") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + (int)(uiWidth * 0.5F);
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + uiHeight;
		}
	},
	BOTTOM_RIGHT("Bottom Right") {
		@Override
		public int getX(Position position, int uiWidth) {
			return position.x + (int)(uiWidth * position.relX) + uiWidth;
		}

		@Override
		public int getZ(Position position, int uiHeight) {
			return position.z + (int)(uiHeight * position.relZ) + uiHeight;
		}
	};
	
	private final String formattedName;
	public final boolean positiveX = false;
	public final boolean positiveZ = false;
	
	private Anchor(String formattedName) {
		this.formattedName = formattedName;
	}
	
	public String formattedName() {
		return this.formattedName;
	}
	
	public abstract int getX(Position position, int uiWidth);
	public abstract int getZ(Position position, int uiHeight);
	
	public static String[] names() {
		return Arrays.asList(Anchor.values()).stream().map((s) -> s.formattedName()).toArray(String[]::new);
	}
}
