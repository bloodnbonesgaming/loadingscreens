package me.platypus.loadingscreens.client.gui.component;

import com.mojang.blaze3d.platform.NativeImage;

import imgui.ImGui;
import imgui.flag.ImGuiInputTextFlags;
import imgui.type.ImString;
import me.platypus.loadingscreens.client.ImageUtil;
import net.minecraft.Util;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.resources.ResourceLocation;

public class TextureComponent extends ComponentBase {

	public String texturePath = "";
	public transient ResourceLocation texture = TextureManager.INTENTIONAL_MISSING_TEXTURE;
	public boolean tile = false;
	public int tileWidth = 32;
	public int tileHeight = 32;
	public Size size = new Size();
	
	public transient int textureWidth;
	public transient int textureHeight;
	public transient int frames;
	
	@Override
	public void onLoad() {
		if (this.texturePath.isBlank()) {
			this.texture = TextureManager.INTENTIONAL_MISSING_TEXTURE;
		} else {
			this.texture = new ResourceLocation(this.texturePath);
			this.loadTextureInfo();
		}
	}
	
	private void loadTextureInfo() {
		NativeImage img = ImageUtil.loadNativeImage(Minecraft.getInstance().getResourceManager(), this.texture);
		
		if (img != null) {
			this.textureWidth = img.getWidth();
			this.textureHeight = img.getHeight();
			this.frames = this.textureHeight / this.textureWidth;
		} else {
			this.textureWidth = 0;
			this.textureHeight = 0;
			this.frames = 0;
		}
	}

	@Override
	public void render(GuiGraphics graphics, int x, int z, int uiWidth, int uiHeight, float partialTick) {
		int width = this.size.width(uiWidth);
		int height = this.size.height(uiHeight);
		int xPos = this.position.getXPos(uiWidth, width);
		int zPos = this.position.getZPos(uiHeight, height);
		
		try {
			if (this.frames > 1) {
				int frame = (int)(Util.getMillis() / (this.frames * 1) % this.frames);
				int fullWidth = width;
				int fullHeight = height;
				int textureRenderWidth;
				int textureRenderHeight;
				
				if (tile) {
					textureRenderWidth = tileWidth;
					textureRenderHeight = tileHeight * this.frames;
				} else {
					textureRenderWidth = width;
					textureRenderHeight = height * this.frames;
				}
				graphics.blit(this.texture, xPos, zPos, 0, 0.0F, frame * (textureRenderHeight * 1F / this.frames), fullWidth, fullHeight, textureRenderWidth, textureRenderHeight);
			} else {
				if (tile) {
					graphics.blit(this.texture, xPos, zPos, 0, 0.0F, 0.0F, width, height, tileWidth, tileHeight);
				} else {
					graphics.blit(this.texture, xPos, zPos, 0, 0.0F, 0.0F, width, height, width, height);
				}
			}
		} catch (Exception e) {
			//location might error, don't want to spam the log - it totally still does it though
		}
	}

	@Override
	public void imGui() {
		//Position
    	if (ImGui.treeNode("Position")) {
			this.position.imGui();
        	ImGui.treePop();
        }
    	
    	//Size
    	if (ImGui.treeNode("Size")) {
        	this.size.imGui();
        	ImGui.treePop();
        }
		
        ImString backgroundLocation = new ImString(300);
        backgroundLocation.set(this.texture.toString());
        
        if (ImGui.inputText("Texture", backgroundLocation, ImGuiInputTextFlags.EnterReturnsTrue)) {
        	try {
        		this.texture = new ResourceLocation(backgroundLocation.get());
        		this.texturePath = backgroundLocation.get();
        		this.loadTextureInfo();
        	} catch(Exception e) {
        		
        	}
        }
        
        if (ImGui.radioButton("Tile", this.tile)) {
        	this.tile = !this.tile;
        }
        if (this.tile) {
        	ImString tileWidth = new ImString(Integer.toString(this.tileWidth),  4);
        	ImString tileHeight = new ImString(Integer.toString(this.tileHeight),  4);
        	
        	if (ImGui.inputText("Tile Width", tileWidth, ImGuiInputTextFlags.CharsDecimal | ImGuiInputTextFlags.EnterReturnsTrue)) {
        		try {
            		this.tileWidth = (int) Float.parseFloat(tileWidth.get());
            	} catch(Exception e) {
            		
            	}
        	}
        	if (ImGui.inputText("Tile Height", tileHeight, ImGuiInputTextFlags.CharsDecimal | ImGuiInputTextFlags.EnterReturnsTrue)) {
        		try {
            		this.tileHeight = (int) Float.parseFloat(tileHeight.get());
            	} catch(Exception e) {
            		
            	}
        	}
        }
	}

	@Override
	public String imGuiLabel() {
		return "Texture";
	}

	@Override
	public ComponentBase clone() {
		TextureComponent comp = (TextureComponent) ComponentTypes.TEXTURE.create();
		comp.position = this.position.clone();
		
		comp.texturePath = this.texturePath;
		comp.tile = this.tile;
		comp.tileWidth = this.tileWidth;
		comp.tileHeight = this.tileHeight;
		
		comp.size = this.size.clone();
		
		comp.onLoad();
		
		return comp;
	}
}
