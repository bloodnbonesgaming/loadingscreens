package me.platypus.loadingscreens.client;

import me.platypus.loadingscreens.client.config.ConfigManager;
import me.platypus.loadingscreens.client.gui.screens.ConnectScreenOverride;
import me.platypus.loadingscreens.client.gui.screens.GenericDirtMessageScreenOverride;
import me.platypus.loadingscreens.client.gui.screens.LevelLoadingScreenOverride;
import me.platypus.loadingscreens.client.gui.screens.ProgressScreenOverride;
import me.platypus.loadingscreens.client.gui.screens.ReceivingLevelScreenOverride;
import net.minecraft.client.gui.screens.ConnectScreen;
import net.minecraft.client.gui.screens.DisconnectedScreen;
import net.minecraft.client.gui.screens.GenericDirtMessageScreen;
import net.minecraft.client.gui.screens.LevelLoadingScreen;
import net.minecraft.client.gui.screens.ProgressScreen;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraftforge.client.event.ScreenEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class ClientEventHandler {

	@SubscribeEvent(priority=EventPriority.HIGHEST)
	public void OnOpenGui(final ScreenEvent.Opening event) {
		final Screen screen = event.getNewScreen();
						
		if (screen instanceof ProgressScreen castScreen && !(screen instanceof ProgressScreenOverride)) {
			event.setNewScreen(new ProgressScreenOverride(castScreen.clearScreenAfterStop));
		} else if (screen instanceof LevelLoadingScreen castScreen && !(screen instanceof LevelLoadingScreenOverride)) {
			event.setNewScreen(new LevelLoadingScreenOverride(castScreen.progressListener));
		} else if (screen instanceof GenericDirtMessageScreen castScreen && !(screen instanceof GenericDirtMessageScreenOverride)) {
			event.setNewScreen(new GenericDirtMessageScreenOverride(castScreen.getTitle()));
		} else if (screen instanceof ReceivingLevelScreen castScreen && !(screen instanceof ReceivingLevelScreenOverride)) {
			event.setNewScreen(new ReceivingLevelScreenOverride());
		} else if (screen instanceof ConnectScreen castScreen && !(screen instanceof ConnectScreenOverride)) {
			event.setNewScreen(new ConnectScreenOverride(castScreen.parent, castScreen.connectFailedTitle));
		} else if (screen instanceof TitleScreen || screen instanceof DisconnectedScreen) {
			ConfigManager.randomizeText();
		}
	}
}
